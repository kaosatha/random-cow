#!/bin/sh

set -e

if [ -z "$COWPATH" ]; then
    COWPATH='/usr/local/share/cows'
fi

getcowfile() {
    # saving `ls` on freebsd does not save as newline separated list. rather,
    # acts like `ls` in terminal mode - ie space separated list
    cows=`ls -1 $COWPATH`
    ncows="`ls -1 $COWPATH | wc -l`"
    random=`od -An -N2 -i /dev/random`
    index=`dc -e "$random $ncows % f"`
    echo $cows | awk "{ print \$$index }"
}

getfileext() {
    echo `echo $1 | cut -d '.' -f 2`
}

COWFILE=`getcowfile`
COWEXT=`getfileext $COWFILE`

while test ! $COWEXT = "cow" ; do
    COWFILE=`getcowfile`
    COWEXT=`getfileext $COWFILE`
done

COW=`echo $COWFILE | cut -d '.' -f 1`

fortune -a | cowsay -f $COW

